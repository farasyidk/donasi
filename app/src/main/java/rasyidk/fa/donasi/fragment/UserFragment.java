package rasyidk.fa.donasi.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import rasyidk.fa.donasi.LoginActivity;
import rasyidk.fa.donasi.R;
import rasyidk.fa.donasi.UserSession;
import rasyidk.fa.donasi.model.Blogs;
import rasyidk.fa.donasi.ui.MainPresenter;

public class UserFragment extends Fragment {

    public UserFragment() {

    }

    EditText txtJudul, txtFoto, txtDesc;
    Button btnPublish;
    MainPresenter mainPresenter;
    Blogs datas;
    UserSession session;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_user, container, false);
        setHasOptionsMenu(true);
        session = new UserSession(getContext());

        txtJudul = view.findViewById(R.id.txtJudulDonasi);
        txtDesc = view.findViewById(R.id.txtDesc);
        txtFoto = view.findViewById(R.id.txtFoto);
        btnPublish = view.findViewById(R.id.btnPublish);

        mainPresenter = new MainPresenter();

        btnPublish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datas = new Blogs("judul", "desc", "fotohh.jpg", "03");
                mainPresenter.postBlogs(datas);
            }
        });


        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflator) {

        inflator.inflate(R.menu.logout_menu, menu);
        super.onCreateOptionsMenu(menu, inflator);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        switch (item.getItemId()) {
            case R.id.logout_item:
                session.logoutUser();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }
}
