package rasyidk.fa.donasi.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import rasyidk.fa.donasi.R;
import rasyidk.fa.donasi.adapter.DashboardAdapter;
import rasyidk.fa.donasi.model.BlogsResponse;
import rasyidk.fa.donasi.ui.MainPresenter;
import rasyidk.fa.donasi.ui.MainViewInterface;

public class DashboardFragment extends Fragment implements MainViewInterface {

    public DashboardFragment() {

    }

    RecyclerView rv_dashboard;

    String TAG = "DashboardFragment";
    RecyclerView.Adapter adapter;
    MainPresenter mainPresenter;
    Context ctx;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        ctx = view.getContext();
        rv_dashboard = view.findViewById(R.id.rv_dashboard);

        mainPresenter = new MainPresenter(this);
        rv_dashboard.setLayoutManager(new LinearLayoutManager(ctx));
        mainPresenter.getBlogs();

        return view;
    }

    @Override
    public void showToast(String str) {
        Toast.makeText(ctx,str,Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayBlogs(BlogsResponse blogsResponse) {
        if (blogsResponse != null) {
            Log.d(TAG, String.valueOf(blogsResponse.getHasil()));
            adapter = new DashboardAdapter(blogsResponse.getHasil(), ctx);
            rv_dashboard.setAdapter(adapter);
        } else {
            Log.d(TAG,"Blogs response null");
        }
    }

    @Override
    public void displayError(String s) {
        showToast(s);
    }
}
