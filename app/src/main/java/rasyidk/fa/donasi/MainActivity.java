package rasyidk.fa.donasi;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import rasyidk.fa.donasi.fragment.DashboardFragment;
import rasyidk.fa.donasi.fragment.UserFragment;

public class MainActivity extends AppCompatActivity {

    ActionBar toolbar;
    BottomNavigationView bottomNavigation;
    UserSession session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        session = new UserSession(this);
        session.checkLogin();

        toolbar = getSupportActionBar();
        bottomNavigation = findViewById(R.id.navigationView);
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        initFragment(new DashboardFragment());
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_dashboard:
                    initFragment(new DashboardFragment());
                    return true;
                case R.id.navigation_user:
                    initFragment(new UserFragment());
                    return true;
            }
            return false;
        }
    };

    private void initFragment(Fragment classFragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, classFragment);
        transaction.commit();
    }
}
