package rasyidk.fa.donasi;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import rasyidk.fa.donasi.ui.MainPresenter;

public class LoginActivity extends AppCompatActivity {

    Button btnLogin;
    EditText edtUser;
    EditText edtPassword;
    MainPresenter mainPresenter;
    TextView txtTest;

    private static final String PREFER_NAME = "Reg";
    UserSession session;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnLogin = findViewById(R.id.btn_login);
        edtUser = findViewById(R.id.input_username);
        edtPassword = findViewById(R.id.input_password);
        txtTest = findViewById(R.id.txtTest);

        mainPresenter = new MainPresenter(this);

        session = new UserSession(getApplicationContext());
        sharedPreferences = getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtUser.getText().toString() != "" && edtPassword.getText().toString() != "") {
                    mainPresenter.requestLogin(edtUser.getText().toString(), edtPassword.getText().toString(), session);
                }
            }
        });
    }
}
