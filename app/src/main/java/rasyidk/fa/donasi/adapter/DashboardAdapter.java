package rasyidk.fa.donasi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import rasyidk.fa.donasi.R;
import rasyidk.fa.donasi.model.Blogs;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.DashboardViewHolder> {
    private ArrayList<Blogs> dataList;
    private Context context;

    public DashboardAdapter(ArrayList<Blogs> dataList, Context context) {
        this.dataList = dataList;
        this.context = context;
    }

    @Override
    public DashboardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.card_blogs, parent, false);
        return new DashboardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DashboardViewHolder holder, int position) {
        holder.txtJudul.setText(dataList.get(position).getJudul());
        holder.txtDesc.setText(dataList.get(position).getDesc());

        Glide.with(context).load(dataList.get(position).getFoto()).into(holder.imgBlogs);
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class DashboardViewHolder extends RecyclerView.ViewHolder{
        private TextView txtJudul, txtDesc;
        private ImageView imgBlogs;

        public DashboardViewHolder(View itemView) {
            super(itemView);
            txtJudul = itemView.findViewById(R.id.txt_judul);
            txtDesc = itemView.findViewById(R.id.txt_desc);
            imgBlogs = itemView.findViewById(R.id.imgBlogs);
        }
    }
}
