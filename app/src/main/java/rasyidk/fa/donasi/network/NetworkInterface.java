package rasyidk.fa.donasi.network;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import rasyidk.fa.donasi.model.Blogs;
import rasyidk.fa.donasi.model.BlogsResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface NetworkInterface {

    @GET("campaign")
    Observable<BlogsResponse> getBlogs();

    @POST("campaign")
    @Multipart
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    Observable<Blogs> postBlogs(@Field("judul") String judul,
                                @Field("isi") String isi,
                                @Field("foto") String foto);

    @POST("auth/signin")
    @FormUrlEncoded
    Call<ResponseBody> loginRequest(@Field("name") String name,
                                    @Field("password") String password);
}
