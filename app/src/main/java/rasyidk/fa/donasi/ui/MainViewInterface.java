package rasyidk.fa.donasi.ui;

import rasyidk.fa.donasi.model.BlogsResponse;

public interface MainViewInterface {
    void showToast(String s);
    void displayBlogs(BlogsResponse blogsResponse);
    void displayError(String s);
}
