package rasyidk.fa.donasi.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import rasyidk.fa.donasi.LoginActivity;
import rasyidk.fa.donasi.MainActivity;
import rasyidk.fa.donasi.UserSession;

public class LoginView {


    void showToast(Context context, String s) {
        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
    }
    void displaySukses(Context context, UserSession session, String key) {
        session.createUserLoginSession(key);

        showToast(context, "berhasil login");
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

}
